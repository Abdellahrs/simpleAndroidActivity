package com.rsmouki.zed.tp_1;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class HelloActivity extends AppCompatActivity {

    ImageView returnImg;
    TextView mesg;
    String message;
    String name,stat;
    private  static final String TAG="HelloActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello);

        returnImg = (ImageView)findViewById(R.id.backArrow);
        mesg = (TextView)findViewById(R.id.Message);

        Intent intent = getIntent();
        stat = intent.getStringExtra("stat");
        name = intent.getStringExtra("name");
        if(stat.equals("no data entered")) {
            message = "Hello"+ getResources().getString(R.string.introduction_sentence);
        }else{
            message = "Hello "+name+""+ getResources().getString(R.string.introduction_sentence);
        }


        final ProgressDialog progressDialog = new ProgressDialog(HelloActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Handling input...");
        progressDialog.show();
        // TODO: Implementing data and waiting for tree second
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() { progressDialog.dismiss();
                    }
                }, 5000);

        mesg.setText(message);

        returnImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"Return to Main Activity...");
                Intent intent = new Intent(HelloActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG,"on Destroy");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG,"On Start");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG,"on Stop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG,"On Resume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG,"on Pause");
    }
}
