package com.rsmouki.zed.tp_1;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private  static  final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText input = (EditText)findViewById(R.id.editText);



        Button startAct = (Button)findViewById(R.id.button);

        startAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = input.getText().toString();
                Intent intent = new Intent(MainActivity.this,HelloActivity.class);
                if(name.equals("Name")) {
                    intent.putExtra("stat","no data entered");
                }else{
                    intent.putExtra("stat","data entered");
                    intent.putExtra("name",name);
                }

                Log.d(TAG, "starting HelloActivity..");
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG,"on Destroy");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG,"On Start");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG,"on Stop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG,"On Resume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG,"on Pause");
    }


}
